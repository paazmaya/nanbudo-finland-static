# nanbudo-finland-static

![GhostInspector status](https://api.ghostinspector.com/v1/suites/5492b649be2180fe4d5aeae1/status-badge)

Static version of the website for the Finnish Nanbudo Federation, as dynamically build sites are so 2000.

## License

Creative Commons 3 ShareAlike
