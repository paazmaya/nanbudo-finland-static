/**
 * @package Seichusen
 * @author Juga Paazmaya <olavic@gmail.com>
 * @link http://paazmaya.com
 * @license Creative Commons 3 ShareAlike
 */
(function runOnce() {
  'use strict';

  /**
   * Hover handler for SNBL logo
   * @param {Event} event Hover event
   * @returns {void} Nothing
   */
  var logoMenuToggle = function logoMenuToggle(event) {
    event.preventDefault();

    var hideClass = 'js-hidden';
    var la = document.querySelector('.loginarea');
    if (la.classList.contains(hideClass)) {
      la.classList.remove(hideClass);
    }
    else {
      la.classList.add(hideClass);
    }
  };

  // handle login and language selection list
  var lb = document.querySelector('.logo-button a');
  lb.addEventListener('mouseover', logoMenuToggle);
  lb.addEventListener('click', logoMenuToggle);
})();
