/**
 * @author Juga Paazmaya <olavic@gmail.com>
 * @link https://paazmaya.fi
 * @license Creative Commons 3 ShareAlike
 */

'use strict';

module.exports = function gruntConf(grunt) {
  require('time-grunt')(grunt); // Must be first item
  require('jit-grunt')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    imagemin: {
      content: {
        options: {
          optimizationLevel: 3
        },
        files: [
          {
            expand: true,
            cwd: 'public/images/',
            src: ['*.png'],
            dest: 'public/images/'
          }
        ]
      }
    },

    uglify: {
      options: {
        preserveComments: 'some',
        banner: '/*! nanbudo.fi <%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        report: 'min',
        sourceMap: true
      },
      dist: {
        files: {
          'public/js/nanbudo.fi.min.js': [
            'source/js/seichusen.js'
          ],
          'public/js/analytics.min.js': [
            'source/js/analytics.js'
          ]
        }
      }
    },

    autoprefixer: {
      options: {
        browsers: ['last 3 versions'],
        cascade: false
      },
      originals: {
        src: 'source/css/*.css'
      }
    },

    cssmin: {
      dist: {
        options: {
          banner: '/*! nanbudo.fi <%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */',
          report: 'min'
        },
        files: {
          'public/css/nanbudo.fi.min.css': [
            'node_modules/purecss/build/pure.css',
            'node_modules/purecss/build/grids-responsive.css',
            'source/css/layout.css'
          ]
        }
      }
    },

    watch: {
      javascript: {
        files: ['source/js/*.js'],
        tasks: ['uglify'],
        options: {
          spawn: false
        }
      },
      css: {
        files: ['source/css/*.css'],
        tasks: ['mincss'],
        options: {
          spawn: false
        }
      }
    }

  });

  grunt.registerTask('default', ['minify']);

  grunt.registerTask('mincss', ['autoprefixer', 'cssmin']);
  grunt.registerTask('minify', ['uglify', 'mincss']);

};
